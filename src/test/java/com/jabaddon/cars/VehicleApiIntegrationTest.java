package com.jabaddon.cars;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jabaddon.cars.manufacturers.domain.Manufacturer;
import com.jabaddon.cars.manufacturers.domain.Model;
import com.jabaddon.cars.manufacturers.domain.Trim;
import com.jabaddon.cars.manufacturers.domain.service.ManufacturerService;
import com.jabaddon.cars.vehicles.domain.TransmissionType;
import com.jabaddon.cars.vehicles.domain.Vehicle;
import com.jayway.jsonpath.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VehicleApiIntegrationTest extends BaseIntegrationTest {

    @Autowired
    private ManufacturerService manufacturerService;

    @Test
    public void getAllVehiclesAsUser() {
        long count = asStream(vehicleService.findAll()).count();

        HttpEntity<Object> requestEntity = new HttpEntity<>(headersWithAuthenticationForUser());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.GET, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        Object json = Configuration.defaultConfiguration().jsonProvider().parse(responseEntity.getBody());
        assertThat(json, isJson());
        assertThat(json, hasJsonPath("$._embedded"));
        assertThat(json, hasJsonPath("$._embedded.vehicleList", hasSize((int) count)));
    }

    @Test
    public void getAllVehiclesAsAdmin() {
        long count = asStream(vehicleService.findAll()).count();

        HttpEntity<Object> requestEntity = new HttpEntity<>(headersWithAuthenticationForAdmin());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.GET, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        Object json = Configuration.defaultConfiguration().jsonProvider().parse(responseEntity.getBody());
        assertThat(json, isJson());
        assertThat(json, hasJsonPath("$._embedded"));
        assertThat(json, hasJsonPath("$._embedded.vehicleList", hasSize((int) count)));
    }

    @Test
    public void getOneVehiclesAsUser() {
        Vehicle vehicle = asStream(vehicleService.findAll()).findFirst().get();

        HttpEntity<Object> requestEntity = new HttpEntity<>(headersWithAuthenticationForUser());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles/" + vehicle.getId()), HttpMethod.GET, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        Object json = Configuration.defaultConfiguration().jsonProvider().parse(responseEntity.getBody());
        assertThat(json, isJson());
        assertThat(json, hasJsonPath("$.id", is(vehicle.getId())));
        assertThat(json, hasJsonPath("$.numDoors", is(vehicle.getNumDoors())));
        assertThat(json, hasJsonPath("$.transmissionType", is(vehicle.getTransmissionType().name())));
    }

    @Test
    public void deleteOneVehicleAsUser() {
        Vehicle vehicle = asStream(vehicleService.findAll()).findFirst().get();
        long countBefore = asStream(vehicleService.findAll()).count();

        HttpEntity<Object> requestEntity = new HttpEntity<>(headersWithAuthenticationForUser());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles/" + vehicle.getId()), HttpMethod.DELETE, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NO_CONTENT));

        long countAfter = asStream(vehicleService.findAll()).count();
        assertThat(countAfter, is(countBefore-1));
    }

    @Test
    public void updateOneVehicleAsUser() {
        Vehicle vehicle = asStream(vehicleService.findAll()).findFirst().get();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("numDoors", 7);
        objectNode.put("numSeats", 100);
        objectNode.put("transmissionType", "manual");
        HttpEntity<Object> requestEntity = new HttpEntity<>(objectNode.toString(), headersWithAuthenticationForUser());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles/" + vehicle.getId()), HttpMethod.PUT, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.CREATED));

        Vehicle updatedVehicle = vehicleService.findByIdOrFail(vehicle.getId());
        assertThat(updatedVehicle.getNumDoors(), is(7));
        assertThat(updatedVehicle.getNumSeats(), is(100));
        assertThat(updatedVehicle.getTransmissionType(), is(TransmissionType.MANUAL));
    }

    @Test
    public void createNewVehicleAsUser() throws IOException {
        Manufacturer toyota = manufacturerService.findById(1L).get();
        Model prius = asStream(toyota.getModels()).filter(m -> m.getName().equals("Prius")).findFirst().get();
        Trim trim = prius.getTrims().stream().findAny().get();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("year", 2018);
        objectNode.put("numDoors", 4);
        objectNode.put("numSeats", 5);
        objectNode.put("transmissionType", "manual");
        objectNode.put("airCond", true);
        objectNode.putObject("fuelCapacity").put("capacity", 15).put("unit", "gallons");
        objectNode.putObject("engine").put("numCylinders", 4).putObject("fuelType").put("id", 1);
        objectNode.putObject("manufacturer").put("id", toyota.getId());
        objectNode.putObject("model").put("id", prius.getId());
        objectNode.putObject("trim").put("id", trim.getId());
        HttpEntity<Object> requestEntity = new HttpEntity<>(objectNode.toString(), headersWithAuthenticationForUser());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.POST, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.CREATED));
        HttpEntity<Object> newRequestEntity = new HttpEntity<>(headersWithAuthenticationForUser());
        ResponseEntity<String> createdEntity = restTemplate.exchange(responseEntity.getHeaders().getLocation(), HttpMethod.GET, newRequestEntity, String.class);
        assertThat(createdEntity.getStatusCode(), is(HttpStatus.OK));
        JsonNode createdEntityJson = objectMapper.readTree(createdEntity.getBody());
        Vehicle newVehicle = vehicleService.findByIdOrFail(createdEntityJson.get("id").asText());
        assertThat(newVehicle.getYear(), is(createdEntityJson.get("year").asInt()));
        assertThat(newVehicle.getNumDoors(), is(createdEntityJson.get("numDoors").asInt()));
        assertThat(newVehicle.getNumSeats(), is(createdEntityJson.get("numSeats").asInt()));
        assertThat(newVehicle.getTransmissionType().name(), is(createdEntityJson.get("transmissionType").asText()));
        assertThat(newVehicle.getAirCond(), is(createdEntityJson.get("airCond").asBoolean()));
        assertThat(newVehicle.getFuelCapacity().getCapacity(), is(createdEntityJson.path("fuelCapacity").path("capacity").asInt()));
        assertThat(newVehicle.getFuelCapacity().getUnit().name(), is(createdEntityJson.path("fuelCapacity").path("unit").asText()));
        assertThat(newVehicle.getEngine().getNumCylinders(), is(createdEntityJson.path("engine").path("numCylinders").asInt()));
        assertThat(newVehicle.getEngine().getFuelType().getId(), is(createdEntityJson.path("engine").path("fuelType").path("id").asLong()));
        assertThat(newVehicle.getEngine().getFuelType().getName(), is(createdEntityJson.path("engine").path("fuelType").path("name").asText()));
        assertThat(newVehicle.getEngine().getFuelType().getCode(), is(createdEntityJson.path("engine").path("fuelType").path("code").asText()));
        assertThat(newVehicle.getManufacturer().getId(), is(createdEntityJson.path("manufacturer").path("id").asLong()));
        assertThat(newVehicle.getManufacturer().getName(), is(createdEntityJson.path("manufacturer").path("name").asText()));
        assertThat(newVehicle.getModel().getId(), is(createdEntityJson.path("model").path("id").asLong()));
        assertThat(newVehicle.getModel().getName(), is(createdEntityJson.path("model").path("name").asText()));
        assertThat(newVehicle.getTrim().getId(), is(createdEntityJson.path("trim").path("id").asLong()));
        assertThat(newVehicle.getTrim().getName(), is(createdEntityJson.path("trim").path("name").asText()));
        // users cannot see this link
        assertThat(createdEntityJson.path("_links").get("approveVehicle"), is(nullValue()));

        vehicleService.deleteById(newVehicle.getId());
    }

    @Test
    public void createNewVehicleAsAdmin() throws IOException {
        Manufacturer toyota = manufacturerService.findById(1L).get();
        Model prius = asStream(toyota.getModels()).filter(m -> m.getName().equals("Prius")).findFirst().get();
        Trim trim = prius.getTrims().stream().findAny().get();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("year", 2018);
        objectNode.put("numDoors", 4);
        objectNode.put("numSeats", 5);
        objectNode.put("transmissionType", "manual");
        objectNode.put("airCond", true);
        objectNode.putObject("fuelCapacity").put("capacity", 15).put("unit", "gallons");
        objectNode.putObject("engine").put("numCylinders", 4).putObject("fuelType").put("id", 1);
        objectNode.putObject("manufacturer").put("id", toyota.getId());
        objectNode.putObject("model").put("id", prius.getId());
        objectNode.putObject("trim").put("id", trim.getId());
        HttpEntity<Object> requestEntity = new HttpEntity<>(objectNode.toString(), headersWithAuthenticationForAdmin());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.POST, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.CREATED));
        HttpEntity<Object> newRequestEntity = new HttpEntity<>(headersWithAuthenticationForAdmin());
        ResponseEntity<String> createdEntity = restTemplate.exchange(responseEntity.getHeaders().getLocation(), HttpMethod.GET, newRequestEntity, String.class);
        assertThat(createdEntity.getStatusCode(), is(HttpStatus.OK));
        JsonNode createdEntityJson = objectMapper.readTree(createdEntity.getBody());
        Vehicle newVehicle = vehicleService.findByIdOrFail(createdEntityJson.get("id").asText());
        assertThat(newVehicle.getYear(), is(createdEntityJson.get("year").asInt()));
        assertThat(newVehicle.getNumDoors(), is(createdEntityJson.get("numDoors").asInt()));
        assertThat(newVehicle.getNumSeats(), is(createdEntityJson.get("numSeats").asInt()));
        assertThat(newVehicle.getTransmissionType().name(), is(createdEntityJson.get("transmissionType").asText()));
        assertThat(newVehicle.getAirCond(), is(createdEntityJson.get("airCond").asBoolean()));
        assertThat(newVehicle.getFuelCapacity().getCapacity(), is(createdEntityJson.path("fuelCapacity").path("capacity").asInt()));
        assertThat(newVehicle.getFuelCapacity().getUnit().name(), is(createdEntityJson.path("fuelCapacity").path("unit").asText()));
        assertThat(newVehicle.getEngine().getNumCylinders(), is(createdEntityJson.path("engine").path("numCylinders").asInt()));
        assertThat(newVehicle.getEngine().getFuelType().getId(), is(createdEntityJson.path("engine").path("fuelType").path("id").asLong()));
        assertThat(newVehicle.getEngine().getFuelType().getName(), is(createdEntityJson.path("engine").path("fuelType").path("name").asText()));
        assertThat(newVehicle.getEngine().getFuelType().getCode(), is(createdEntityJson.path("engine").path("fuelType").path("code").asText()));
        assertThat(newVehicle.getManufacturer().getId(), is(createdEntityJson.path("manufacturer").path("id").asLong()));
        assertThat(newVehicle.getManufacturer().getName(), is(createdEntityJson.path("manufacturer").path("name").asText()));
        assertThat(newVehicle.getModel().getId(), is(createdEntityJson.path("model").path("id").asLong()));
        assertThat(newVehicle.getModel().getName(), is(createdEntityJson.path("model").path("name").asText()));
        assertThat(newVehicle.getTrim().getId(), is(createdEntityJson.path("trim").path("id").asLong()));
        assertThat(newVehicle.getTrim().getName(), is(createdEntityJson.path("trim").path("name").asText()));
        // users cannot see this link
        assertThat(createdEntityJson.path("_links").get("approveVehicle"), is(notNullValue()));

        vehicleService.deleteById(newVehicle.getId());
    }

    @Test
    public void createNewVehicleWithIncorrectlyManufacturerAsAdmin() throws IOException {
        Manufacturer toyota = manufacturerService.findById(1L).get();
        Model prius = asStream(toyota.getModels()).filter(m -> m.getName().equals("Prius")).findFirst().get();
        Trim trim = prius.getTrims().stream().findAny().get();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("year", 2018);
        objectNode.put("numDoors", 4);
        objectNode.put("numSeats", 5);
        objectNode.put("transmissionType", "manual");
        objectNode.put("airCond", true);
        objectNode.putObject("fuelCapacity").put("capacity", 15).put("unit", "gallons");
        objectNode.putObject("engine").put("numCylinders", 4).putObject("fuelType").put("id", 1);
        objectNode.putObject("manufacturer").put("id", 1000L);
        objectNode.putObject("model").put("id", prius.getId());
        objectNode.putObject("trim").put("id", trim.getId());
        HttpEntity<Object> requestEntity = new HttpEntity<>(objectNode.toString(), headersWithAuthenticationForAdmin());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.POST, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));

        JsonNode responseJson = objectMapper.readTree(responseEntity.getBody());
        assertThat(responseJson.path("message").asText(), is("Manufacturer 1000 is not valid"));
    }

    @Test
    public void createNewVehicleWithIncorrectlyModelAsAdmin() throws IOException {
        Manufacturer toyota = manufacturerService.findById(1L).get();
        Model prius = asStream(toyota.getModels()).filter(m -> m.getName().equals("Prius")).findFirst().get();
        Trim trim = prius.getTrims().stream().findAny().get();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("year", 2018);
        objectNode.put("numDoors", 4);
        objectNode.put("numSeats", 5);
        objectNode.put("transmissionType", "manual");
        objectNode.put("airCond", true);
        objectNode.putObject("fuelCapacity").put("capacity", 15).put("unit", "gallons");
        objectNode.putObject("engine").put("numCylinders", 4).putObject("fuelType").put("id", 1);
        objectNode.putObject("manufacturer").put("id", toyota.getId());
        objectNode.putObject("model").put("id", 9);
        objectNode.putObject("trim").put("id", trim.getId());
        HttpEntity<Object> requestEntity = new HttpEntity<>(objectNode.toString(), headersWithAuthenticationForAdmin());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.POST, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));

        JsonNode responseJson = objectMapper.readTree(responseEntity.getBody());
        assertThat(responseJson.path("message").asText(), is("Model 9 is not valid"));
    }

    @Test
    public void createNewVehicleWithIncorrectlyTrimAsAdmin() throws IOException {
        Manufacturer toyota = manufacturerService.findById(1L).get();
        Model prius = asStream(toyota.getModels()).filter(m -> m.getName().equals("Prius")).findFirst().get();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("year", 2018);
        objectNode.put("numDoors", 4);
        objectNode.put("numSeats", 5);
        objectNode.put("transmissionType", "manual");
        objectNode.put("airCond", true);
        objectNode.putObject("fuelCapacity").put("capacity", 15).put("unit", "gallons");
        objectNode.putObject("engine").put("numCylinders", 4).putObject("fuelType").put("id", 1);
        objectNode.putObject("manufacturer").put("id", toyota.getId());
        objectNode.putObject("model").put("id", prius.getId());
        objectNode.putObject("trim").put("id", 7);
        HttpEntity<Object> requestEntity = new HttpEntity<>(objectNode.toString(), headersWithAuthenticationForAdmin());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.POST, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));

        JsonNode responseJson = objectMapper.readTree(responseEntity.getBody());
        assertThat(responseJson.path("message").asText(), is("Trim 7 is not valid"));
    }

    @Test
    public void createNewVehicleWithYearDataFormatAsAdmin() throws IOException {
        Manufacturer toyota = manufacturerService.findById(1L).get();
        Model prius = asStream(toyota.getModels()).filter(m -> m.getName().equals("Prius")).findFirst().get();
        Trim trim = prius.getTrims().stream().findAny().get();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("year", "Not a valid year!");
        objectNode.put("numDoors", 4);
        objectNode.put("numSeats", 5);
        objectNode.put("transmissionType", "manual");
        objectNode.put("airCond", true);
        objectNode.putObject("fuelCapacity").put("capacity", 15).put("unit", "gallons");
        objectNode.putObject("engine").put("numCylinders", 4).putObject("fuelType").put("id", 1);
        objectNode.putObject("manufacturer").put("id", toyota.getId());
        objectNode.putObject("model").put("id", prius.getId());
        objectNode.putObject("trim").put("id", trim.getId());
        HttpEntity<Object> requestEntity = new HttpEntity<>(objectNode.toString(), headersWithAuthenticationForAdmin());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.POST, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));

        JsonNode responseJson = objectMapper.readTree(responseEntity.getBody());
        assertThat(responseJson.path("message").asText(),
                is("Cannot deserialize value of type `java.lang.Integer` from String \"Not a valid year!\": not a valid Integer value at 'year'"));
    }

    @Test
    public void createNewVehicleWithIncorrectFuelCapacityAsAdmin() throws IOException {
        Manufacturer toyota = manufacturerService.findById(1L).get();
        Model prius = asStream(toyota.getModels()).filter(m -> m.getName().equals("Prius")).findFirst().get();
        Trim trim = prius.getTrims().stream().findAny().get();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("year", 2017);
        objectNode.put("numDoors", 4);
        objectNode.put("numSeats", 5);
        objectNode.put("transmissionType", "manual");
        objectNode.put("airCond", true);
        objectNode.put("fuelCapacity", 1);
        objectNode.putObject("engine").put("numCylinders", 4).putObject("fuelType").put("id", 1);
        objectNode.putObject("manufacturer").put("id", toyota.getId());
        objectNode.putObject("model").put("id", prius.getId());
        objectNode.putObject("trim").put("id", trim.getId());
        HttpEntity<Object> requestEntity = new HttpEntity<>(objectNode.toString(), headersWithAuthenticationForAdmin());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.POST, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));

        JsonNode responseJson = objectMapper.readTree(responseEntity.getBody());
        assertThat(responseJson.path("message").asText(), is("JSON parse error at 'fuelCapacity'"));
    }

    @Test
    public void createNewVehicleWithIncorrectEngineAsAdmin() throws IOException {
        Manufacturer toyota = manufacturerService.findById(1L).get();
        Model prius = asStream(toyota.getModels()).filter(m -> m.getName().equals("Prius")).findFirst().get();
        Trim trim = prius.getTrims().stream().findAny().get();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("year", 2017);
        objectNode.put("numDoors", 4);
        objectNode.put("numSeats", 5);
        objectNode.put("transmissionType", "manual");
        objectNode.put("airCond", true);
        objectNode.putObject("fuelCapacity").put("capacity", 15).put("unit", "gallons");
        objectNode.put("engine", "incorrect engine");
        objectNode.putObject("manufacturer").put("id", toyota.getId());
        objectNode.putObject("model").put("id", prius.getId());
        objectNode.putObject("trim").put("id", trim.getId());
        HttpEntity<Object> requestEntity = new HttpEntity<>(objectNode.toString(), headersWithAuthenticationForAdmin());
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/internal/vehicles"), HttpMethod.POST, requestEntity, String.class);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));

        JsonNode responseJson = objectMapper.readTree(responseEntity.getBody());
        assertThat(responseJson.path("message").asText(), is("JSON parse error at 'engine'"));
    }

}
