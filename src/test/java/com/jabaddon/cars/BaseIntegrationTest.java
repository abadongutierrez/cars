package com.jabaddon.cars;

import com.jabaddon.cars.vehicles.domain.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public abstract class BaseIntegrationTest {
    @Autowired
    protected VehicleService vehicleService;

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    String url(String uri) {
        return "http://localhost:" + port + uri;
    }

    protected MultiValueMap<String, String> headersWithAuthenticationForViewer() {
        MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
        valueMap.set(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForTim");
        valueMap.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        return valueMap;
    }

    protected MultiValueMap<String, String> headersWithAuthenticationForUser() {
        MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
        valueMap.set(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForWoz");
        valueMap.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        return valueMap;
    }

    protected MultiValueMap<String, String> headersWithAuthenticationForAdmin() {
        MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
        valueMap.set(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForSteve");
        valueMap.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        return valueMap;
    }

    protected <T> Stream<T> asStream(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
