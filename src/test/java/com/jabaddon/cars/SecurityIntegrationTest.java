package com.jabaddon.cars;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = CarsApplication.class)
@AutoConfigureMockMvc
public class SecurityIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void callingPublicUrlShouldNotReturnUnauthorized() throws Exception {
        mvc.perform(post("/api/public/login")
            .param("username", "steve").param("password", "12345")
        ).andExpect(status().isOk());
    }

    @Test
    public void callingNonPublicUrlsShouldReturnUnauthorized() throws Exception {
        mvc.perform(get("/api/vehicles")).andExpect(status().isUnauthorized());
        mvc.perform(get("/api/vehicles/123")).andExpect(status().isUnauthorized());
        mvc.perform(get("/api/internal/fueltypes")).andExpect(status().isUnauthorized());
        mvc.perform(get("/api/internal/fueltypes/1")).andExpect(status().isUnauthorized());
        mvc.perform(get("/api/internal/manufactures")).andExpect(status().isUnauthorized());
        mvc.perform(get("/api/internal/manufactures/1")).andExpect(status().isUnauthorized());
        mvc.perform(get("/api/internal/vehicles")).andExpect(status().isUnauthorized());
        mvc.perform(get("/api/internal/vehicles/12345")).andExpect(status().isUnauthorized());
    }

    @Test
    public void onlyViewersAreAllowed() throws Exception {
        mvc.perform(get("/api/vehicles")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForTim")
        ).andExpect(status().isOk());

        mvc.perform(get("/api/vehicles")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForSteve")
        ).andExpect(status().isForbidden());

        mvc.perform(get("/api/vehicles")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForWoz")
        ).andExpect(status().isForbidden());
    }

    @Test
    public void viewersAreNotAllowed() throws Exception {
        mvc.perform(get("/api/internal/vehicles")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForTim")
        ).andExpect(status().isForbidden());

        mvc.perform(get("/api/internal/fueltypes")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForTim")
        ).andExpect(status().isForbidden());

        mvc.perform(get("/api/internal/manufacturers")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForTim")
        ).andExpect(status().isForbidden());

        mvc.perform(get("/api/internal/vehicles")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForWoz")
        ).andExpect(status().isOk());

        mvc.perform(get("/api/internal/fueltypes")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForWoz")
        ).andExpect(status().isOk());

        mvc.perform(get("/api/internal/manufacturers")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForWoz")
        ).andExpect(status().isOk());

        mvc.perform(get("/api/internal/vehicles")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForSteve")
        ).andExpect(status().isOk());

        mvc.perform(get("/api/internal/fueltypes")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForSteve")
        ).andExpect(status().isOk());

        mvc.perform(get("/api/internal/manufacturers")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForSteve")
        ).andExpect(status().isOk());
    }

    @Test
    public void onlyAdminCanApprove() throws Exception {
        mvc.perform(post("/api/internal/vehicles/12345/approve")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForTim")
        ).andExpect(status().isForbidden());

        mvc.perform(post("/api/internal/vehicles/12345/approve")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForWoz")
        ).andExpect(status().isForbidden());

        mvc.perform(post("/api/internal/vehicles/12345/approve")
                .header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForSteve")
        ).andExpect(status().isNotFound());
    }
}
