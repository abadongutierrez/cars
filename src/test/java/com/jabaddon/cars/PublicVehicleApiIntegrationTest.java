package com.jabaddon.cars;

import com.jabaddon.cars.manufacturers.domain.Manufacturer;
import com.jabaddon.cars.vehicles.domain.Vehicle;
import com.jabaddon.cars.vehicles.domain.VehicleStatus;
import com.jabaddon.cars.vehicles.domain.service.VehicleService;
import com.jayway.jsonpath.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PublicVehicleApiIntegrationTest extends BaseIntegrationTest {

    @Test
    public void getAllVehiclesShouldReturnNothingSinceNoneVehicleAreApproved() {
        ResponseEntity<String> responseEntity = getVehicles("/api/vehicles");

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        Object json = Configuration.defaultConfiguration().jsonProvider().parse(responseEntity.getBody());
        assertThat(json, isJson());
        assertThat(json, hasNoJsonPath("$._embedded"));
        assertThat(json, hasNoJsonPath("$._embedded.vehicleList"));
    }

    @Test
    public void getAllVehiclesShouldReturnOneApprovedVehicle() {
        Vehicle vehicle = vehicleService.findAll().iterator().next();
        vehicleService.approve(vehicle.getId());

        ResponseEntity<String> responseEntity = getVehicles("/api/vehicles");

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        Object json = Configuration.defaultConfiguration().jsonProvider().parse(responseEntity.getBody());
        assertThat(json, isJson());
        assertThat(json, hasJsonPath("$._embedded"));
        assertThat(json, hasJsonPath("$._embedded.vehicleList"));
        assertThat(json, hasJsonPath("$._embedded.vehicleList", hasSize(1)));
        assertThat(json, hasJsonPath("$._embedded.vehicleList[0].id", is(vehicle.getId())));
        // viewers cannot see the status
        assertThat(json, hasNoJsonPath("$._embedded.vehicleList[0].status"));
    }

    @Test
    public void getAllVehiclesByYear() {
        setAllVehiclesInReview();
        // approve only those with year 2015
        List<Vehicle> collect = asStream(vehicleService.findAll())
                .filter(v -> v.getYear().equals(2015)).collect(Collectors.toList());
        collect.forEach(v -> vehicleService.approve(v.getId()));
        int count = collect.size();

        ResponseEntity<String> responseEntity = getVehicles("/api/vehicles?year=2015");

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        Object json = Configuration.defaultConfiguration().jsonProvider().parse(responseEntity.getBody());
        assertThat(json, isJson());
        assertThat(json, hasJsonPath("$._embedded"));
        assertThat(json, hasJsonPath("$._embedded.vehicleList"));
        assertThat(json, hasJsonPath("$._embedded.vehicleList", hasSize(count)));
    }

    @Test
    public void getAllVehiclesByManufacturerName() {
        setAllVehiclesInReview();
        // approve only Toyota vehicles
        List<Vehicle> collect = asStream(vehicleService.findAll())
                .filter(v -> v.getManufacturer().getName().equals(Manufacturer.TOYOTA_NAME)).collect(Collectors.toList());
        collect.forEach(v -> vehicleService.approve(v.getId()));
        int count = collect.size();

        ResponseEntity<String> responseEntity = getVehicles("/api/vehicles?manufacturerNameLike=Toyo");

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        Object json = Configuration.defaultConfiguration().jsonProvider().parse(responseEntity.getBody());
        assertThat(json, isJson());
        assertThat(json, hasJsonPath("$._embedded"));
        assertThat(json, hasJsonPath("$._embedded.vehicleList"));
        assertThat(json, hasJsonPath("$._embedded.vehicleList", hasSize(count)));
    }

    private ResponseEntity<String> getVehicles(String s) {
        HttpEntity<Object> requestEntity = new HttpEntity<>(headersWithAuthenticationForViewer());
        return restTemplate.exchange(url(s), HttpMethod.GET, requestEntity, String.class);
    }

    private void setAllVehiclesInReview() {
        // set all vehicles to IN_REVIEW
        // need to remove and save because save creates a new record with a new uuid based on the vehicle in its argument
        asStream(vehicleService.findAll())
                .forEach(v -> {
                    vehicleService.deleteById(v.getId());
                    vehicleService.save(v.toBuilder().status(VehicleStatus.IN_REVIEW).build());
                });
    }

}
