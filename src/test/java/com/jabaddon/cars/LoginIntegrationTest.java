package com.jabaddon.cars;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LoginIntegrationTest extends BaseIntegrationTest {

    @Test
    public void loginAsSteve() {
        executeLoginAs("steve", "complexTokenForSteve");
    }

    @Test
    public void loginAsWoz() {
        executeLoginAs("woz", "complexTokenForWoz");
    }

    @Test
    public void loginAsTim() {
        executeLoginAs("tim", "complexTokenForTim");
    }

    private void executeLoginAs(String steve, String complexTokenForSteve) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("username", steve);
        map.add("password", "12345");
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(url("/api/public/login"), HttpMethod.POST, entity, String.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(responseEntity.getBody(), is(complexTokenForSteve));
    }
}
