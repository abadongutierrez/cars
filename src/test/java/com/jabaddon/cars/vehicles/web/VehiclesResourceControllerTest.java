package com.jabaddon.cars.vehicles.web;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jabaddon.cars.CarsApplication;
import com.jabaddon.cars.vehicles.domain.FluidMeasurementUnit;
import com.jabaddon.cars.vehicles.domain.TransmissionType;
import com.jabaddon.cars.vehicles.domain.Vehicle;
import com.jabaddon.cars.vehicles.domain.service.VehicleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = CarsApplication.class)
@AutoConfigureMockMvc
public class VehiclesResourceControllerTest {

    private static final String API_VEHICLES_URI = "/api/internal/vehicles";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VehicleService vehicleService;

    @Before
    public void before() {
    }

    @Test
    public void testFindAllMapping() throws Exception {

        when(vehicleService.findAll()).thenReturn(new ArrayList<>());

        mockMvc.perform(_get(API_VEHICLES_URI)).andExpect(status().isOk());
        mockMvc.perform(_get(API_VEHICLES_URI + "/")).andExpect(status().isOk());
    }

    @Test
    public void getAllVehicles() throws Exception {
        when(vehicleService.findAll(any())).thenReturn(Arrays.asList(
                Vehicle.builder().id("1").build(),
                Vehicle.builder().id("2").build(),
                Vehicle.builder().id("3").build()
        ));

        mockMvc.perform(_get(API_VEHICLES_URI))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
                .andExpect(jsonPath("$._embedded.vehicleList", hasSize(3)))
                .andExpect(jsonPath("$._embedded.vehicleList[0].id", is("1")))
                .andExpect(jsonPath("$._embedded.vehicleList[1].id", is("2")))
                .andExpect(jsonPath("$._embedded.vehicleList[2].id", is("3")));

        verify(vehicleService).findAll(any());
        verifyNoMoreInteractions(vehicleService);
    }

    @Test
    public void getVehicle() throws Exception {
        when(vehicleService.findByIdOrFail("1")).thenReturn(Vehicle.builder().id("1").build());

        mockMvc.perform(_get(API_VEHICLES_URI + "/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
                .andExpect(jsonPath("$.id", is("1")));

        verify(vehicleService).findByIdOrFail("1");
        verifyNoMoreInteractions(vehicleService);
    }

    @Test
    public void deleteVehicle() throws Exception {
        mockMvc.perform(_delete(API_VEHICLES_URI + "/1"))
                .andExpect(status().isNoContent());

        verify(vehicleService).deleteById("1");
        verifyNoMoreInteractions(vehicleService);
    }

    @Test
    public void postVehicle() throws Exception {
        ArgumentCaptor<Vehicle> vehicleArgumentCaptor = ArgumentCaptor.forClass(Vehicle.class);
        when(vehicleService.save(vehicleArgumentCaptor.capture())).thenReturn(Vehicle.builder().id("1").build());

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(this.getClass().getResource("VehiclesResourceControllerTest_postVehicle.json"));

        mockMvc.perform(_post(API_VEHICLES_URI).content(jsonNode.toString()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        Vehicle vehicle = vehicleArgumentCaptor.getValue();
        assertThat(vehicle.getYear(), is(1999));
        assertThat(vehicle.getNumDoors(), is(4));
        assertThat(vehicle.getNumSeats(), is(5));
        assertThat(vehicle.getAirCond(), is(true));
        assertThat(vehicle.getTransmissionType(), is(TransmissionType.AUTO));
        assertThat(vehicle.getEngine().getNumCylinders(), is(4));
        assertThat(vehicle.getEngine().getFuelType().getId(), is(1L));
        assertThat(vehicle.getFuelCapacity().getCapacity(), is(100));
        assertThat(vehicle.getFuelCapacity().getUnit(), is(FluidMeasurementUnit.LITERS));
        assertThat(vehicle.getManufacturer().getId(), is(2L));
        assertThat(vehicle.getModel().getId(), is(202L));
        assertThat(vehicle.getTrim().getId(), is(2022L));
        
        verify(vehicleService).save(any());
        verifyNoMoreInteractions(vehicleService);
    }

    @Test
    public void whenJsonObjectIsExpectedItShouldReturnJsonParseError() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(
                this.getClass().getResource("VehiclesResourceControllerTest_whenJsonObjectIsExpectedItShouldReturnJsonParseError.json"));

        mockMvc.perform(_post(API_VEHICLES_URI).content(jsonNode.toString()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("JSON parse error at 'manufacturer'")));
    }

    @Test
    public void whenAnSpecificDataTypeIsExpectedItShouldReturnError() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(
                this.getClass().getResource("VehiclesResourceControllerTest_whenAnSpecificDataTypeIsExpectedItShouldReturnError.json"));

        mockMvc.perform(_post(API_VEHICLES_URI).content(jsonNode.toString()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Cannot deserialize value of type `java.lang.Integer` from String \"this is not a valid year!\":" +
                        " not a valid Integer value at 'year'")));
    }

    private MockHttpServletRequestBuilder _get(String uri) {
        return get(uri).header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForSteve");
    }

    private MockHttpServletRequestBuilder _post(String uri) {
        return post(uri).header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForSteve");
    }

    private MockHttpServletRequestBuilder _delete(String uri) {
        return delete(uri).header(HttpHeaders.AUTHORIZATION, "Bearer complexTokenForSteve");
    }
}