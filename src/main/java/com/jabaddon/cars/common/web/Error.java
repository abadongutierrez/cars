package com.jabaddon.cars.common.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.jabaddon.cars.vehicles.domain.Vehicle;
import lombok.Builder;
import lombok.Value;
import org.springframework.validation.FieldError;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Value
@Builder
@JsonDeserialize(builder = Vehicle.VehicleBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {
    private int status;
    private String message;
    private List<CustomFieldError> fieldErrors;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<CustomFieldError> getFieldErrors() {
        return fieldErrors;
    }

    public Date getTimestamp() {
        return new Date();
    }

    public static Error fromFieldErrors(List<FieldError> fieldErrors) {
        Error.ErrorBuilder errorBuilder = Error.builder()
                .status(BAD_REQUEST.value())
                .message("Validation errors");
        errorBuilder.fieldErrors(fieldErrors.stream().map(fieldError -> CustomFieldError.builder()
                .field(fieldError.getField())
                .message(fieldError.getDefaultMessage())
                .build())
                .collect(Collectors.toList()));
        return errorBuilder.build();
    }
}
