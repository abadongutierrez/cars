package com.jabaddon.cars.common.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonDeserialize(builder = CustomFieldError.CustomFieldErrorBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomFieldError {
    private String field;
    private String message;
}