package com.jabaddon.cars.config;

import com.jabaddon.cars.fueltypes.domain.FuelType;
import com.jabaddon.cars.fueltypes.domain.service.FuelTypeService;
import com.jabaddon.cars.manufacturers.domain.Manufacturer;
import com.jabaddon.cars.manufacturers.domain.Model;
import com.jabaddon.cars.manufacturers.domain.Trim;
import com.jabaddon.cars.manufacturers.domain.service.ManufacturerService;
import com.jabaddon.cars.users.domain.User;
import com.jabaddon.cars.users.domain.repository.UserRepository;
import com.jabaddon.cars.vehicles.domain.*;
import com.jabaddon.cars.vehicles.domain.service.VehicleService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Optional;

@Configuration
public class DatabaseInitializer {

    @Bean
    CommandLineRunner initVehicles(VehicleService vehicleService,
                                   ManufacturerService manufacturerService,
                                   FuelTypeService fuelTypeService,
                                   UserRepository userRepository) {
        return args -> {
            createFuelTypes(fuelTypeService);
            createManufacturers(manufacturerService);
            createVehicles(vehicleService, fuelTypeService, manufacturerService);

            /*
            ADMIN - manage vehicles, see status in vehicles and change status
            USER - manage vehicles, see status but cannot change status
            VIEWER - only query vehicles using /api/vehicles/**
             */
            userRepository.save(User.builder().id(1L).username("steve").password("12345")
                    .token("complexTokenForSteve").role("ADMIN").build());
            userRepository.save(User.builder().id(2L).username("woz").password("12345")
                    .token("complexTokenForWoz").role("USER").build());
            userRepository.save(User.builder().id(3L).username("tim").password("12345")
                    .token("complexTokenForTim").role("VIEWER").build());
        };
    }

    private void createVehicles(VehicleService vehicleService,
                                FuelTypeService fuelTypeService,
                                ManufacturerService manufacturerService) {
        Manufacturer toyota = manufacturerService.findByIdOrFail(1L);
        Manufacturer vw = manufacturerService.findByIdOrFail(2L);

        Optional<Model> camryModel = toyota.getModels().stream().filter(model -> model.getId().equals(101L)).findFirst();
        Optional<Trim> camryModelTrim1 = camryModel.get().getTrims().stream().filter(trim -> trim.getId().equals(1011L)).findFirst();
        Optional<Model> priusModel = toyota.getModels().stream().filter(model -> model.getId().equals(102L)).findFirst();
        Optional<Trim> priusModelTrim1 = priusModel.get().getTrims().stream().filter(trim -> trim.getId().equals(1021L)).findFirst();

        Optional<Model> beetleModel = vw.getModels().stream().filter(model -> model.getId().equals(201L)).findFirst();
        Optional<Trim> beetleModelTrim1 = beetleModel.get().getTrims().stream().filter(trim -> trim.getId().equals(2011L)).findFirst();
        Optional<Model> jettaModel = vw.getModels().stream().filter(model -> model.getId().equals(202L)).findFirst();
        Optional<Trim> jettaModelTrim1 = jettaModel.get().getTrims().stream().filter(trim -> trim.getId().equals(2021L)).findFirst();

        Vehicle v1 = Vehicle.builder()
                .year(2015).numDoors(4).airCond(true).numSeats(5).transmissionType(TransmissionType.AUTO)
                .engine(Engine.builder().fuelType(fuelTypeService.findByIdOrFail(1L)).numCylinders(4).build())
                .fuelCapacity(FluidMeasurement.builder().capacity(20).unit(FluidMeasurementUnit.GALLONS).build())
                .manufacturer(toyota)
                .model(camryModel.get())
                .trim(camryModelTrim1.get())
                .build();
        vehicleService.save(v1);

        Vehicle v2 = Vehicle.builder()
                .year(2015).numDoors(4).airCond(true).numSeats(5).transmissionType(TransmissionType.AUTO)
                .engine(Engine.builder().fuelType(fuelTypeService.findByIdOrFail(2L)).numCylinders(4).build())
                .fuelCapacity(FluidMeasurement.builder().capacity(10).unit(FluidMeasurementUnit.GALLONS).build())
                .manufacturer(toyota)
                .model(priusModel.get())
                .trim(priusModelTrim1.get())
                .build();
        vehicleService.save(v2);

        Vehicle v3 = Vehicle.builder()
                .year(2016).numDoors(4).airCond(true).numSeats(5).transmissionType(TransmissionType.MANUAL)
                .engine(Engine.builder().fuelType(fuelTypeService.findByIdOrFail(1L)).numCylinders(4).build())
                .fuelCapacity(FluidMeasurement.builder().capacity(20).unit(FluidMeasurementUnit.GALLONS).build())
                .manufacturer(vw)
                .model(beetleModel.get())
                .trim(beetleModelTrim1.get())
                .build();
        vehicleService.save(v3);

        Vehicle v4 = Vehicle.builder()
                .year(2017).numDoors(4).airCond(true).numSeats(5).transmissionType(TransmissionType.AUTO)
                .engine(Engine.builder().fuelType(fuelTypeService.findByIdOrFail(1L)).numCylinders(4).build())
                .fuelCapacity(FluidMeasurement.builder().capacity(22).unit(FluidMeasurementUnit.GALLONS).build())
                .manufacturer(vw)
                .model(jettaModel.get())
                .trim(jettaModelTrim1.get())
                .build();
        vehicleService.save(v4);
    }

    private void createManufacturers(ManufacturerService manufacturerService) {
        Manufacturer.ManufacturerBuilder builder = Manufacturer.builder();
        Manufacturer toyotaManufacturer = builder.id(1L)
                .name(Manufacturer.TOYOTA_NAME)
                .models(Arrays.asList(
                        Model.builder().id(101L).name("Camry")
                                .trims(Arrays.asList(
                                        Trim.builder().id(1011L).name("LE 4dr Sedan (2.5L 4cyl 6A)").build(),
                                        Trim.builder().id(1012L).name("SE 4dr Sedan (2.5L 4cyl 6A)").build()
                                )).build(),
                        Model.builder().id(102L).name("Prius")
                                .trims(Arrays.asList(
                                        Trim.builder().id(1021L).name("Five 4dr Hatchback (1.8L 4cyl gas/electric hybrid CVT)").build(),
                                        Trim.builder().id(1022L).name("Four 4dr Hatchback (1.8L 4cyl gas/electric hybrid CVT)").build()
                                )).build()
                ))
                .build();
        Manufacturer vwManufacturer = builder.id(2L)
                .name("Volkswagen")
                .models(Arrays.asList(
                        Model.builder().id(201L).name("Beetle")
                                .trims(Arrays.asList(
                                        Trim.builder().id(2011L).name("1.8T 2dr Hatchback (1.8L 4cyl Turbo 5M)").build(),
                                        Trim.builder().id(2012L).name("1.8T 2dr Hatchback (1.8L 4cyl Turbo 6A)").build()
                                )).build(),
                        Model.builder().id(202L).name("Jetta")
                                .trims(Arrays.asList(
                                        Trim.builder().id(2021L).name("4dr Sedan (2.0L 4cyl 5M)").build(),
                                        Trim.builder().id(2022L).name("GLI SE 4dr Sedan (2.0L 4cyl Turbo 6A)").build()
                                )).build()
                ))
                .build();
        manufacturerService.save(toyotaManufacturer);
        manufacturerService.save(vwManufacturer);
    }

    private void createFuelTypes(FuelTypeService fuelTypeService) {
        fuelTypeService.save(FuelType.builder().name("Petrol").code("PETROL").build());
        fuelTypeService.save(FuelType.builder().name("Hybrid").code("HYBRID").build());
        fuelTypeService.save(FuelType.builder().name("Electric").code("ELECTRIC").build());
        fuelTypeService.save(FuelType.builder().name("Diesel").code("DIESEL").build());
    }
}
