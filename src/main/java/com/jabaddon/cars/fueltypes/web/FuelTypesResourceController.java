package com.jabaddon.cars.fueltypes.web;

import com.jabaddon.cars.fueltypes.domain.FuelType;
import com.jabaddon.cars.fueltypes.domain.service.FuelTypeNotFoundException;
import com.jabaddon.cars.fueltypes.domain.service.FuelTypeService;
import com.jabaddon.cars.vehicles.domain.service.VehicleNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.jabaddon.cars.common.web.Error;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/api/internal/fueltypes")
public class FuelTypesResourceController implements ResourceAssembler<FuelType, Resource<FuelType>> {

    private final FuelTypeService fuelTypeService;

    @Autowired
    public FuelTypesResourceController(FuelTypeService fuelTypeService) {
        this.fuelTypeService = fuelTypeService;
    }

    @GetMapping(path = {"/", ""})
    Resources<Resource<FuelType>> getAll() {
        List<Resource<FuelType>> fuelTypes = StreamSupport.stream(fuelTypeService.findAll().spliterator(), false)
                .map(this::toResource)
                .collect(Collectors.toList());
        return new Resources<>(fuelTypes,
                linkTo(methodOn(FuelTypesResourceController.class).getAll()).withSelfRel());
    }

    @GetMapping("/{id}")
    Resource<FuelType> getOne(@PathVariable Long id) {
        return toResource(fuelTypeService.findByIdOrFail(id));
    }

    @Override
    public Resource<FuelType> toResource(FuelType fuelType) {
        return new Resource<>(fuelType,
                linkTo(methodOn(FuelTypesResourceController.class).getOne(fuelType.getId())).withSelfRel(),
                linkTo(methodOn(FuelTypesResourceController.class).getAll()).withRel("fuelTypes"));
    }

    @ResponseBody
    @ExceptionHandler(FuelTypeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    Error fuelTypeNotFoundExceptionHandler(FuelTypeNotFoundException ex) {
        return Error.builder()
                .status(NOT_FOUND.value())
                .message(String.format("Fuel Type with id %s not found", ex.getMessage()))
                .build();
    }
}
