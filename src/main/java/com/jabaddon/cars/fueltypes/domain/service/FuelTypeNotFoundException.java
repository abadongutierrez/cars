package com.jabaddon.cars.fueltypes.domain.service;

public class FuelTypeNotFoundException extends RuntimeException {
    public FuelTypeNotFoundException() {
        super();
    }

    public FuelTypeNotFoundException(String message) {
        super(message);
    }

    public FuelTypeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FuelTypeNotFoundException(Throwable cause) {
        super(cause);
    }

    protected FuelTypeNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
