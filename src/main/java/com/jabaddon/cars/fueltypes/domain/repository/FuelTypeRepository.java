package com.jabaddon.cars.fueltypes.domain.repository;

import com.jabaddon.cars.fueltypes.domain.FuelType;
import org.springframework.data.repository.CrudRepository;

public interface FuelTypeRepository extends CrudRepository<FuelType, Long> {
}
