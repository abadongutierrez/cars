package com.jabaddon.cars.fueltypes.domain.service.impl;

import com.jabaddon.cars.fueltypes.domain.FuelType;
import com.jabaddon.cars.fueltypes.domain.repository.FuelTypeRepository;
import com.jabaddon.cars.fueltypes.domain.service.FuelTypeNotFoundException;
import com.jabaddon.cars.fueltypes.domain.service.FuelTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
public class FuelTypeServiceImpl implements FuelTypeService {

    private final FuelTypeRepository fuelTypeRepository;

    @Autowired
    public FuelTypeServiceImpl(FuelTypeRepository fuelTypeRepository) {
        this.fuelTypeRepository = fuelTypeRepository;
    }

    @Override
    public Iterable<FuelType> findAll() {
        return fuelTypeRepository.findAll();
    }

    @Override
    public Optional<FuelType> findById(Long id) {
        return fuelTypeRepository.findById(id);
    }

    @Override
    public FuelType findByIdOrFail(Long id) {
        return fuelTypeRepository.findById(id).orElseThrow(() -> new FuelTypeNotFoundException(String.valueOf(id)));
    }

    @Override
    public FuelType save(FuelType fuelType) {
        Long maxId = findMaxId().orElse(0L);
        return fuelTypeRepository.save(fuelType.toBuilder().id(maxId+1).build());
    }

    private Optional<Long> findMaxId() {
        Optional<FuelType> max =
                StreamSupport.stream(findAll().spliterator(), true).max(Comparator.comparing(FuelType::getId));
        return max.map(FuelType::getId);
    }
}
