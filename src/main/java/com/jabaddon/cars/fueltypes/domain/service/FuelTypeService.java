package com.jabaddon.cars.fueltypes.domain.service;

import com.jabaddon.cars.fueltypes.domain.FuelType;

import java.util.Optional;

public interface FuelTypeService {
    Iterable<FuelType> findAll();

    Optional<FuelType> findById(Long id);

    FuelType findByIdOrFail(Long id);

    FuelType save(FuelType vehicle);
}
