package com.jabaddon.cars.fueltypes.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

@Value
@Builder(toBuilder = true)
@JsonDeserialize(builder = FuelType.FuelTypeBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FuelType {
    @Id
    @NotNull
    private Long id;
    private String code;
    private String name;

    // to be able to serialize using the builder and jackson
    @JsonPOJOBuilder(withPrefix = "")
    public static class FuelTypeBuilder {}
}
