package com.jabaddon.cars.users.domain.repository;

import com.jabaddon.cars.users.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
