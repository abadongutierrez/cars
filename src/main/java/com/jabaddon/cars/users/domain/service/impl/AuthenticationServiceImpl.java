package com.jabaddon.cars.users.domain.service.impl;

import com.jabaddon.cars.users.domain.User;
import com.jabaddon.cars.users.domain.repository.UserRepository;
import com.jabaddon.cars.users.domain.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.StreamSupport;

@Component
public class AuthenticationServiceImpl implements AuthenticationService {
    private final UserRepository userRepository;

    @Autowired
    public AuthenticationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<String> login(String username, String password) {
        Optional<User> user = StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .filter(u -> u.getUsername().equals(username))
                .findFirst();
        // NOTE: of course password should be hashed here
        if (user.isPresent() && user.get().getPassword().equals(password)) {
            return Optional.of(user.get().getToken());
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> findUserByToken(String token) {
        return StreamSupport.stream(
                userRepository.findAll().spliterator(), false).filter(u -> u.getToken().equals(token)).findFirst();
    }
}
