package com.jabaddon.cars.users.domain.service;

import com.jabaddon.cars.users.domain.User;

import java.util.Optional;

public interface AuthenticationService {
    Optional<String> login(String username, String password);

    Optional<User> findUserByToken(String token);
}
