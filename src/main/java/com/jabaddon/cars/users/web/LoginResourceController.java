package com.jabaddon.cars.users.web;

import com.jabaddon.cars.users.domain.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/public")
public class LoginResourceController {

    private final AuthenticationService authenticationService;

    @Autowired
    public LoginResourceController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }


    @PostMapping("/login")
    String login(@RequestParam("username") final String username, @RequestParam("password") final String password) {
        return authenticationService
                .login(username, password)
                .orElseThrow(() -> new RuntimeException("Invalid username/password"));
    }
}
