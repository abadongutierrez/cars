package com.jabaddon.cars.vehicles.web;

import com.jabaddon.cars.vehicles.domain.service.VehicleSearch;
import com.jabaddon.cars.vehicles.domain.service.VehicleService;

public abstract class BaseVehicleResourceController {

    final VehicleService vehicleService;

    public BaseVehicleResourceController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    VehicleSearch buildVehicleSearch(Integer year, String manufacturerName) {
        VehicleSearch.VehicleSearchBuilder builder = VehicleSearch.builder();
        builder.year(year);
        builder.manufacturerNameLike(manufacturerName);
        return builder.build();
    }
}
