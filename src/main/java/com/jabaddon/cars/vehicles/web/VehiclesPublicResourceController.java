package com.jabaddon.cars.vehicles.web;

import com.jabaddon.cars.vehicles.domain.Vehicle;
import com.jabaddon.cars.vehicles.domain.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/vehicles")
public class VehiclesPublicResourceController extends BaseVehicleResourceController {

    @Autowired
    VehiclesPublicResourceController(VehicleService vehicleService) {
        super(vehicleService);
    }

    @GetMapping("/{uuid}")
    ResponseEntity<?> getVehicle(@PathVariable String uuid) {
        Vehicle vehicle = vehicleService.findByIdOrFail(uuid);
        if (vehicle.isInReviewStatus()) {
            // Just kidding :D this should return NOT_FOUND or something else because it is not in the correct status
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(toResource(vehicle.toBuilder().status(null).build()));
    }

    @GetMapping(path = {"/", ""})
    Resources<Resource<Vehicle>> getVehicles(@RequestParam(required = false) Integer year,
                                             @RequestParam(required = false) String manufacturerNameLike) {
        List<Resource<Vehicle>> vehicles = StreamSupport.stream(
                vehicleService.findAll(buildVehicleSearch(year, manufacturerNameLike)).spliterator(), false)
                .filter(vehicle -> !vehicle.isInReviewStatus())
                .map(v -> v.toBuilder().status(null).build()) // nullify so it is not visible to VIEWERS
                .map(this::toResource)
                .collect(Collectors.toList());
        return new Resources<>(vehicles,
                linkTo(methodOn(VehiclesPublicResourceController.class).getVehicles(null, null)).withSelfRel());
    }

    public Resource<Vehicle> toResource(Vehicle vehicle) {
        List<Link> links = new ArrayList<>();
        links.add(linkTo(methodOn(VehiclesPublicResourceController.class).getVehicle(vehicle.getId())).withSelfRel());
        links.add(linkTo(methodOn(VehiclesPublicResourceController.class).getVehicles(null, null)).withRel("vehicles"));
        return new Resource<>(vehicle, links);
    }
}
