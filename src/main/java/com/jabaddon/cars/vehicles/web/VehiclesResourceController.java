package com.jabaddon.cars.vehicles.web;

import com.jabaddon.cars.users.domain.User;
import com.jabaddon.cars.vehicles.domain.Vehicle;
import com.jabaddon.cars.vehicles.domain.VehicleStatus;
import com.jabaddon.cars.vehicles.domain.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/internal/vehicles")
public class VehiclesResourceController extends BaseVehicleResourceController {

    @Autowired
    VehiclesResourceController(VehicleService vehicleService) {
        super(vehicleService);
    }

    @GetMapping("/{uuid}")
    Resource<Vehicle> getVehicle(@PathVariable String uuid, @AuthenticationPrincipal final User user) {
        return toResource(vehicleService.findByIdOrFail(uuid), user);
    }

    @GetMapping(path = {"/", ""})
    Resources<Resource<Vehicle>> getVehicles(@RequestParam(required = false) Integer year,
                                             @RequestParam(required = false) String manufacturerNameLike,
                                             @AuthenticationPrincipal final User user) {
        List<Resource<Vehicle>> vehicles = StreamSupport.stream(
                vehicleService.findAll(buildVehicleSearch(year, manufacturerNameLike)).spliterator(), false)
                .map(v -> toResource(v, user))
                .collect(Collectors.toList());
        return new Resources<>(vehicles,
                linkTo(methodOn(VehiclesPublicResourceController.class).getVehicles(null, null)).withSelfRel());
    }

    @DeleteMapping("/{uuid}")
    ResponseEntity<?> deleteVehicle(@PathVariable String uuid) {
        vehicleService.deleteById(uuid);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(path = {"/", ""})
    ResponseEntity<?> saveVehicle(@Valid @RequestBody Vehicle vehicle) throws URISyntaxException {
        Vehicle newVehicle = vehicleService.save(vehicle);
        Resource<Vehicle> vehicleResource = toResource(newVehicle);
        return ResponseEntity.created(new URI(vehicleResource.getId().expand().getHref())).body(vehicleResource);
    }

    @PostMapping("/{uuid}/approve")
    ResponseEntity<?> approveVehicle(@PathVariable String uuid, @AuthenticationPrincipal final User user) {
        if (!user.isAdmin()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        vehicleService.approve(uuid);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/{uuid}")
    ResponseEntity<?> updateVehicle(@PathVariable String uuid, @RequestBody Vehicle vehicle) throws URISyntaxException {
        Resource<Vehicle> vehicleResource = toResource(vehicleService.update(uuid, vehicle));
        return ResponseEntity.created(new URI(vehicleResource.getId().expand().getHref())).body(vehicleResource);
    }

    private Resource<Vehicle> toResource(Vehicle vehicle, User user) {
        Resource<Vehicle> vehicleResource = toResource(vehicle);
        if (vehicle.getStatus() == VehicleStatus.IN_REVIEW && (user != null && user.isAdmin())) {
            vehicleResource.add(linkTo(methodOn(VehiclesResourceController.class).approveVehicle(vehicle.getId(), null)).withRel("approveVehicle"));
        }
        return vehicleResource;
    }

    public Resource<Vehicle> toResource(Vehicle vehicle) {
        List<Link> links = new ArrayList<>();
        links.add(linkTo(methodOn(VehiclesResourceController.class).getVehicle(vehicle.getId(), null)).withSelfRel());
        links.add(linkTo(methodOn(VehiclesResourceController.class).getVehicles(null, null, null)).withRel("vehicles"));
        return new Resource<>(vehicle, links);
    }
}
                                                            