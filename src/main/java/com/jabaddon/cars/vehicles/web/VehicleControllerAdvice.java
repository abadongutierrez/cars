package com.jabaddon.cars.vehicles.web;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.jabaddon.cars.common.web.Error;
import com.jabaddon.cars.vehicles.domain.service.VehicleAlreadyApprovedException;
import com.jabaddon.cars.vehicles.domain.service.VehicleNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class VehicleControllerAdvice {

    @ResponseBody
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    Error illegalArgumentExceptionHandler(IllegalArgumentException ex) {
        return Error.builder()
                .status(BAD_REQUEST.value())
                .message(ex.getMessage())
                .build();
    }

    @ResponseBody
    @ExceptionHandler(VehicleNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    Error vehicleNotFoundExceptionHandler(VehicleNotFoundException ex) {
        return Error.builder()
                .status(NOT_FOUND.value())
                .message(String.format("Vehicle with id %s not found", ex.getMessage()))
                .build();
    }

    @ResponseBody
    @ExceptionHandler(VehicleAlreadyApprovedException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    Error alreadyApprovedExceptionHandler(VehicleAlreadyApprovedException ex) {
        return Error.builder()
                .status(HttpStatus.CONFLICT.value())
                .message(ex.getMessage())
                .build();
    }

    @ResponseBody
    @ExceptionHandler(value = InvalidFormatException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    Error invalidFormatExceptionHandler(InvalidFormatException ex) {
        return Error.builder()
                .status(BAD_REQUEST.value())
                .message(ex.getLocalizedMessage().split("\n")[0] + " at '" + collectFieldPath(ex) + "'")
                .build();
    }

    @ResponseBody
    @ExceptionHandler(MismatchedInputException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    Error mismatchedInputExceptionHandler(MismatchedInputException ex) {
        return Error.builder()
                .status(BAD_REQUEST.value())
                .message("JSON parse error at '" + collectFieldPath(ex) + "'")
                .build();
    }

    private String collectFieldPath(MismatchedInputException ex) {
        return ex.getPath().stream().map(JsonMappingException.Reference::getFieldName)
                .collect(Collectors.joining("."));
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public Error methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException ex) {
        return Error.fromFieldErrors(ex.getBindingResult().getFieldErrors());
    }
}
