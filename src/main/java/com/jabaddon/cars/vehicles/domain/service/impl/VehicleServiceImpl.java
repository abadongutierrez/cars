package com.jabaddon.cars.vehicles.domain.service.impl;

import com.jabaddon.cars.fueltypes.domain.FuelType;
import com.jabaddon.cars.fueltypes.domain.service.FuelTypeService;
import com.jabaddon.cars.manufacturers.domain.Manufacturer;
import com.jabaddon.cars.manufacturers.domain.Model;
import com.jabaddon.cars.manufacturers.domain.Trim;
import com.jabaddon.cars.manufacturers.domain.service.ManufacturerService;
import com.jabaddon.cars.vehicles.domain.Engine;
import com.jabaddon.cars.vehicles.domain.Vehicle;
import com.jabaddon.cars.vehicles.domain.VehicleStatus;
import com.jabaddon.cars.vehicles.domain.repository.VehicleRepository;
import com.jabaddon.cars.vehicles.domain.service.VehicleAlreadyApprovedException;
import com.jabaddon.cars.vehicles.domain.service.VehicleNotFoundException;
import com.jabaddon.cars.vehicles.domain.service.VehicleSearch;
import com.jabaddon.cars.vehicles.domain.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private final FuelTypeService fuelTypeService;
    private final ManufacturerService manufacturerService;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository, FuelTypeService fuelTypeService, ManufacturerService manufacturerService) {
        this.vehicleRepository = vehicleRepository;
        this.fuelTypeService = fuelTypeService;
        this.manufacturerService = manufacturerService;
    }

    @Override
    public Iterable<Vehicle> findAll() {
        return vehicleRepository.findAll();
    }

    @Override
    public Iterable<Vehicle> findAll(VehicleSearch vehicleSearch) {
        if (vehicleSearch.getYear() != null && vehicleSearch.getManufacturerNameLike() != null) {
            return vehicleRepository.findVehicleByYearAndManufacturerNameIsLike(
                    vehicleSearch.getYear(), vehicleSearch.getManufacturerNameLike());
        } else if (vehicleSearch.getYear() != null && vehicleSearch.getManufacturerNameLike() == null) {
            return vehicleRepository.findVehicleByYear(vehicleSearch.getYear());
        } else if (vehicleSearch.getYear() == null && vehicleSearch.getManufacturerNameLike() != null) {
            return vehicleRepository.findVehicleByManufacturerNameIsLike(vehicleSearch.getManufacturerNameLike());
        } else {
            return findAll();
        }
    }

    @Override
    public Optional<Vehicle> findById(String uuid) {
        return vehicleRepository.findById(uuid);
    }

    @Override
    public Vehicle findByIdOrFail(String uuid) {
        return findById(uuid).orElseThrow(() -> new VehicleNotFoundException(uuid));
    }

    @Override
    public void deleteById(String uuid) {
        findByIdOrFail(uuid);
        vehicleRepository.deleteById(uuid);
    }

    @Override
    public Vehicle update(String uuid, Vehicle vehicle) {
        Vehicle oldVehicle = findByIdOrFail(uuid);
        // TODO update only the necessary fields
        Vehicle.VehicleBuilder vehicleBuilder = oldVehicle.toBuilder();
        if (vehicle.getYear() != null) {
            vehicleBuilder.year(vehicle.getYear());
        }
        if (vehicle.getNumDoors() != null) {
            vehicleBuilder.numDoors(vehicle.getNumDoors());
        }
        if (vehicle.getNumSeats() != null) {
            vehicleBuilder.numSeats(vehicle.getNumSeats());
        }
        if (vehicle.getAirCond() != null) {
            vehicleBuilder.airCond(vehicle.getAirCond());
        }
        if (vehicle.getTransmissionType() != null) {
            vehicleBuilder.transmissionType(vehicle.getTransmissionType());
        }
        return vehicleRepository.save(vehicleBuilder.build());
    }

    @Override
    public void approve(String uuid) {
        Vehicle vehicle = findByIdOrFail(uuid);
        if (vehicle.getStatus() == VehicleStatus.APPROVED) {
            throw new VehicleAlreadyApprovedException("Vehicle is already approved");
        }
        vehicleRepository.save(vehicle.toBuilder().status(VehicleStatus.APPROVED).build());
    }

    @Override
    public Iterable<Vehicle> findAllApproved() {
        return StreamSupport.stream(findAll().spliterator(), true)
                .filter(Vehicle::isApprovedStatus).collect(Collectors.toList());
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        Vehicle.VehicleBuilder newVehicleBuilder = vehicle.toBuilder()
                .id(generateUniqueUUID()).status(VehicleStatus.IN_REVIEW);
        // validations
        Long fuelTypeId = vehicle.getEngine().getFuelType().getId();
        Optional<FuelType> fuelType = fuelTypeService.findById(fuelTypeId);
        if (!fuelType.isPresent()) {
            throw new IllegalArgumentException(String.format("Fuel Type %s is not valid", fuelTypeId));
        } else {
            newVehicleBuilder.engine(Engine.builder()
                    .numCylinders(vehicle.getEngine().getNumCylinders()).fuelType(fuelType.get()).build());
        }

        Long manufacturerId = vehicle.getManufacturer().getId();
        Optional<Manufacturer> manufacturerOptional = manufacturerService.findById(manufacturerId);
        newVehicleBuilder.manufacturer(manufacturerOptional.orElseThrow(() ->
                new IllegalArgumentException(String.format("Manufacturer %s is not valid", manufacturerId))));

        Long modelId = vehicle.getModel().getId();
        Optional<Model> modelOptional = manufacturerOptional.get().getModels().stream()
            .filter(m -> m.getId().equals(modelId)).findFirst();
        newVehicleBuilder.model(modelOptional.orElseThrow(() ->
                        new IllegalArgumentException(String.format("Model %s is not valid", modelId))));

        Long trimId = vehicle.getTrim().getId();
        Optional<Trim> trimOptional = modelOptional.get().getTrims().stream().filter(t -> t.getId().equals(trimId))
                .findFirst();
        newVehicleBuilder.trim(trimOptional.orElseThrow(() ->
                new IllegalArgumentException(String.format("Trim %s is not valid", trimId))));

        Vehicle newVehicle = newVehicleBuilder.build();
        return vehicleRepository.save(newVehicle);
    }

    private String generateUniqueUUID() {
        String uuid = UUID.randomUUID().toString();
        for (int i = 0; i < 3; i++) {
            if (!findById(uuid).isPresent()) {
                return uuid;
            }
        }
        throw new RuntimeException("Couldn't generate a unique UUID for Vehicle.");
    }
}
