package com.jabaddon.cars.vehicles.domain.service;

public class VehicleAlreadyApprovedException extends RuntimeException {
    public VehicleAlreadyApprovedException() {
        super();
    }

    public VehicleAlreadyApprovedException(String message) {
        super(message);
    }

    public VehicleAlreadyApprovedException(String message, Throwable cause) {
        super(message, cause);
    }

    public VehicleAlreadyApprovedException(Throwable cause) {
        super(cause);
    }

    protected VehicleAlreadyApprovedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
