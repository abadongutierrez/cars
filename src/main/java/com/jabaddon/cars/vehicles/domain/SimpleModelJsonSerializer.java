package com.jabaddon.cars.vehicles.domain;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.jabaddon.cars.manufacturers.domain.Model;

import java.io.IOException;

public class SimpleModelJsonSerializer extends StdSerializer<Model> {
    public SimpleModelJsonSerializer() {
        this(null);
    }

    public SimpleModelJsonSerializer(Class<Model> t) {
        super(t);
    }

    @Override
    public void serialize(Model model, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", model.getId());
        jsonGenerator.writeStringField("name", model.getName());
        jsonGenerator.writeEndObject();
    }
}
