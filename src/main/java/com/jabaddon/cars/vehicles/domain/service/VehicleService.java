package com.jabaddon.cars.vehicles.domain.service;

import com.jabaddon.cars.vehicles.domain.Vehicle;

import java.util.Optional;

public interface VehicleService {
    Iterable<Vehicle> findAll();

    Iterable<Vehicle> findAll(VehicleSearch vehicleSearch);

    Optional<Vehicle> findById(String uuid);

    Vehicle findByIdOrFail(String uuid);

    void deleteById(String uuid);

    Vehicle save(Vehicle vehicle);

    Vehicle update(String uuid, Vehicle vehicle);

    void approve(String uuid);

    Iterable<Vehicle> findAllApproved();
}
