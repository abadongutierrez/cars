package com.jabaddon.cars.vehicles.domain;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.jabaddon.cars.manufacturers.domain.Manufacturer;

import java.io.IOException;

public class SimpleManufacturerJsonSerializer extends StdSerializer<Manufacturer> {
    public SimpleManufacturerJsonSerializer() {
        this(null);
    }

    public SimpleManufacturerJsonSerializer(Class<Manufacturer> t) {
        super(t);
    }

    @Override
    public void serialize(Manufacturer manufacturer, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", manufacturer.getId());
        jsonGenerator.writeStringField("name", manufacturer.getName());
        jsonGenerator.writeEndObject();
    }
}
