package com.jabaddon.cars.vehicles.domain;

public enum VehicleStatus {
    APPROVED, // Vehicle has been reviewed and its data is approved
    IN_REVIEW // Vehicle is in review
}
