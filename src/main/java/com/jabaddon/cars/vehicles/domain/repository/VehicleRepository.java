package com.jabaddon.cars.vehicles.domain.repository;

import com.jabaddon.cars.vehicles.domain.Vehicle;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VehicleRepository extends CrudRepository<Vehicle, String> {
    List<Vehicle> findVehicleByYear(int year);
    List<Vehicle> findVehicleByManufacturerNameIsLike(String manufacturerNameLike);
    List<Vehicle> findVehicleByYearAndManufacturerNameIsLike(int year, String manufacturerNameLike);
}
