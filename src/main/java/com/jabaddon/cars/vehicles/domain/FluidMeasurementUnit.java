package com.jabaddon.cars.vehicles.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.apache.commons.lang3.StringUtils;

public enum FluidMeasurementUnit {
    LITERS, GALLONS;

    @JsonCreator
    public static FluidMeasurementUnit forValue(String value) {
        try {
            return FluidMeasurementUnit.valueOf(StringUtils.upperCase(value));
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }
}
