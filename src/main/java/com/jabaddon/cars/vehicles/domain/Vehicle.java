package com.jabaddon.cars.vehicles.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jabaddon.cars.manufacturers.domain.Manufacturer;
import com.jabaddon.cars.manufacturers.domain.Model;
import com.jabaddon.cars.manufacturers.domain.Trim;
import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Value
@Builder(toBuilder = true)
@JsonDeserialize(builder = Vehicle.VehicleBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Vehicle {
    @Id
    private String id;
    @NotNull
    @DecimalMin(value = "1950", message = "must be >= 1950")
    private Integer year;
    @NotNull
    @Valid
    private Manufacturer manufacturer;
    @NotNull
    @Valid
    private Model model;
    @NotNull
    @Valid
    private Trim trim;
    @NotNull
    @DecimalMin(value = "2", message = "must be >= 2")
    private Integer numDoors;
    @NotNull
    @DecimalMin(value = "2", message = "must be >= 2")
    private Integer numSeats;
    @NotNull
    @Valid
    private FluidMeasurement fuelCapacity;
    @NotNull(message = "must not be null and one of AUTO|Auto|auto|MANUAL|Manual|manual")
    private TransmissionType transmissionType;
    @NotNull
    private Boolean airCond;
    @NotNull
    @Valid
    private Engine engine;

    private VehicleStatus status;

    // to be able to serialize using the builder and jackson
    @JsonPOJOBuilder(withPrefix = "")
    public static class VehicleBuilder {}

    @JsonSerialize(using = SimpleManufacturerJsonSerializer.class)
    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    @JsonSerialize(using = SimpleModelJsonSerializer.class)
    public Model getModel() {
        return model;
    }

    @JsonIgnore
    public boolean isInReviewStatus() {
        return status != null && status == VehicleStatus.IN_REVIEW;
    }

    @JsonIgnore
    public boolean isApprovedStatus() {
        return status != null && status == VehicleStatus.IN_REVIEW;
    }

    @JsonIgnore
    public String getManufacturerName() {
        return manufacturer.getName();
    }
}
