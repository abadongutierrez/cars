package com.jabaddon.cars.vehicles.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.jabaddon.cars.fueltypes.domain.FuelType;
import lombok.Builder;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Value
@Builder(toBuilder = true)
@JsonDeserialize(builder = Engine.EngineBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Engine {
    @NotNull
    @DecimalMin(value = "1", message = "must be > 0")
    private Integer numCylinders;
    @NotNull
    @Valid
    private FuelType fuelType;

    // to be able to serialize using the builder and jackson
    @JsonPOJOBuilder(withPrefix = "")
    public static class EngineBuilder {
    }
}
