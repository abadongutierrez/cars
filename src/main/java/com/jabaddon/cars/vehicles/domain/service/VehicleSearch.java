package com.jabaddon.cars.vehicles.domain.service;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class VehicleSearch {
    private Integer year;
    private String manufacturerNameLike;
}
