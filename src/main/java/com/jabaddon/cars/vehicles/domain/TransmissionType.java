package com.jabaddon.cars.vehicles.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.apache.commons.lang3.StringUtils;

public enum TransmissionType {
    MANUAL, AUTO;

    @JsonCreator
    public static TransmissionType forValue(String value) {
        try {
            return TransmissionType.valueOf(StringUtils.upperCase(value));
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }
}
