package com.jabaddon.cars.vehicles.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Value
@Builder
@JsonDeserialize(builder = FluidMeasurement.FluidMeasurementBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FluidMeasurement {
    @NotNull
    @DecimalMin(value = "1", message = "must be > 0")
    private Integer capacity;
    @NotNull(message = "must not be null and one of: LITERS|Liters|liters|GALLONS|Gallons|gallons")
    private FluidMeasurementUnit unit;

    // to be able to serialize using the builder and jackson
    @JsonPOJOBuilder(withPrefix = "")
    public static class FluidMeasurementBuilder {}
}
