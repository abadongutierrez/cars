package com.jabaddon.cars.manufacturers.domain.service;

public class ManufacturerNotFoundException extends RuntimeException {
    public ManufacturerNotFoundException() {
        super();
    }

    public ManufacturerNotFoundException(String message) {
        super(message);
    }

    public ManufacturerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ManufacturerNotFoundException(Throwable cause) {
        super(cause);
    }

    protected ManufacturerNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
