package com.jabaddon.cars.manufacturers.domain.service;

import com.jabaddon.cars.manufacturers.domain.Manufacturer;

import java.util.Optional;

public interface ManufacturerService {
    Iterable<Manufacturer> findAll();

    Optional<Manufacturer> findById(Long id);

    Manufacturer findByIdOrFail(Long id);

    Manufacturer save(Manufacturer vehicle);
}
