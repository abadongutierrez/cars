package com.jabaddon.cars.manufacturers.domain.service.impl;

import com.jabaddon.cars.manufacturers.domain.Manufacturer;
import com.jabaddon.cars.manufacturers.domain.repository.ManufacturerRepository;
import com.jabaddon.cars.manufacturers.domain.service.ManufacturerNotFoundException;
import com.jabaddon.cars.manufacturers.domain.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public Iterable<Manufacturer> findAll() {
        return manufacturerRepository.findAll();
    }

    @Override
    public Optional<Manufacturer> findById(Long id) {
        return manufacturerRepository.findById(id);
    }

    @Override
    public Manufacturer findByIdOrFail(Long id) {
        return manufacturerRepository.findById(id)
                .orElseThrow(() -> new ManufacturerNotFoundException(String.valueOf(id)));
    }

    @Override
    public Manufacturer save(Manufacturer vehicle) {
        // just save it, id's are assigned by now
        return manufacturerRepository.save(vehicle);
    }
}
