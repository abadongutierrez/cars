package com.jabaddon.cars.manufacturers.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;

@Value
@Builder(toBuilder = true)
@JsonDeserialize(builder = Trim.TrimBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Trim {
    @Id
    private Long id;
    private String name;

    // to be able to serialize using the builder and jackson
    @JsonPOJOBuilder(withPrefix = "")
    public static class TrimBuilder {}
}
