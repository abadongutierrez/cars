package com.jabaddon.cars.manufacturers.domain.repository;

import com.jabaddon.cars.manufacturers.domain.Manufacturer;
import org.springframework.data.repository.CrudRepository;

public interface ManufacturerRepository extends CrudRepository<Manufacturer, Long> {
}
