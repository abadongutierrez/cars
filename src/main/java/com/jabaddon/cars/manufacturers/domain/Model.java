package com.jabaddon.cars.manufacturers.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;

import java.util.Collection;

@Value
@Builder(toBuilder = true)
@JsonDeserialize(builder = Model.ModelBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Model {
    @Id
    private Long id;
    private String name;
    private Collection<Trim> trims;

    // to be able to serialize using the builder and jackson
    @JsonPOJOBuilder(withPrefix = "")
    public static class ModelBuilder {}
}
