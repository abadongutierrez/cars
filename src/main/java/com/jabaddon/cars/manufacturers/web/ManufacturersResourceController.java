package com.jabaddon.cars.manufacturers.web;

import com.jabaddon.cars.common.web.Error;
import com.jabaddon.cars.fueltypes.domain.FuelType;
import com.jabaddon.cars.fueltypes.domain.service.FuelTypeNotFoundException;
import com.jabaddon.cars.fueltypes.web.FuelTypesResourceController;
import com.jabaddon.cars.manufacturers.domain.Manufacturer;
import com.jabaddon.cars.manufacturers.domain.service.ManufacturerNotFoundException;
import com.jabaddon.cars.manufacturers.domain.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/api/internal/manufacturers")
public class ManufacturersResourceController implements ResourceAssembler<Manufacturer, Resource<Manufacturer>> {

    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturersResourceController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @GetMapping(path = {"/", ""})
    Resources<Resource<Manufacturer>> getAll() {
        List<Resource<Manufacturer>> fuelTypes = StreamSupport.stream(manufacturerService.findAll().spliterator(), false)
                .map(this::toResource)
                .collect(Collectors.toList());
        return new Resources<>(fuelTypes,
                linkTo(methodOn(ManufacturersResourceController.class).getAll()).withSelfRel());
    }

    @GetMapping("/{id}")
    Resource<Manufacturer> getOne(@PathVariable Long id) {
        return toResource(manufacturerService.findByIdOrFail(id));
    }

    @Override
    public Resource<Manufacturer> toResource(Manufacturer fuelType) {
        return new Resource<>(fuelType,
                linkTo(methodOn(ManufacturersResourceController.class).getOne(fuelType.getId())).withSelfRel(),
                linkTo(methodOn(ManufacturersResourceController.class).getAll()).withRel("manufacturers"));
    }

    @ResponseBody
    @ExceptionHandler(ManufacturerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    Error manufacturerNotFoundExceptionHandler(ManufacturerNotFoundException ex) {
        return Error.builder()
                .status(NOT_FOUND.value())
                .message(String.format("Manufacturer with id %s not found", ex.getMessage()))
                .build();
    }
}
