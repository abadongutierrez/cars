# Cars

## Run the app

Build the app:

    mvn build -DskipTests

execute the jar as follows:

    java -jar target/cars-0.0.1-SNAPSHOT.jar
    
and then the app will be listening at port `9999`.

## Some curl commands to test quickly

To login user `steve`:

```
curl -X POST \
  http://localhost:9999/api/public/login \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'cache-control: no-cache' \
  -d 'username=steve&password=12345'
```

To get the fuel types catalog:

```
curl -X GET \
  http://localhost:9999/api/internal/fueltypes \
  -H 'Authorization: Bearer complexTokenForSteve' \
  -H 'cache-control: no-cache'
```

To get the Manufacturers catalog:

```
curl -X GET \
  http://localhost:9999/api/internal/manufacturers \
  -H 'Authorization: Bearer complexTokenForSteve' \
  -H 'cache-control: no-cache'
```

To get the vehicles catalog:

```
curl -X GET \
 http://localhost:9999/api/internal/vehicles \
 -H 'Authorization: Bearer complexTokenForSteve' \
 -H 'cache-control: no-cache'
```


## Tests

To run the tests you should have maven installed and run:

    mvn test

I'm using maven version 3.5.x.

## Context/Use Case

The scenario is something like this:

This is a little API from a Company called 'XYZ Inc' that keeps records of Vehicles and developed this API
to be used by its partners to query or update the Vehicle Catalog.

Other companies can query the Vehicle catalog using to the URL: `/api/vehicles`, but to be able to use
this URL users need credentials and a valid role to use it.

The XYZ Inc. company also exposes a URL `/api/internal/vehicles` to manage the Vehicle catalog, that internal teams/projects
can use it to find, add, delete and update vehicles.

Since a vehicle is associated with the fuel type it uses and the Manufacturer and its different vehicles models. The XYZ Inc.
company also exposes internal URLs `/api/internal/fueltypes` and `/api/internal/manufacturers`  to query the available fuel type
and manufacturers in the catalog.

#### About the Vehicle model

A Vehicle model has attributes to describe the general characteristics of a Car like the year, number of doors, number of seats,
transmission type, fuel tank capacity and also characteristics of its engine like the fuel type and number of cylinders.

Also a Vehicle is associated to the Manufacturer and the specific model of the manufacturer. The combination of manufacturer,
model, trim and year creates specific Vehicle data.

There are 3 types of users `viewer`, `user`, and `admin`. `viewer` can only query the catalog and only those records that
are `approved` by the `admin` user. This is why Vehicle model has an attribute called `status`.

`user`'s manage the vehicle catalog adding, removing, updating the vehicle records but only `admin` users can change the
status `IN_REVIEW` of a Vehicle record to `APPROVED`.

## Notes

In my research I found a lot more of attributes that can be associated to a Vehicle like its dimensions,
specific engine characteristics, etc. but I had to define a narrowed scope and start the development of this exercise to be able
to finish on time. 

I started implementing my own InMemory repositories, later I found out that there is Spring Data KeyValue with default
implementation using Maps and decided to use it (I deleted a lot of code that was not necessary).

For the fuel types and manufacturer catalogs I created an API just to query that data. I wanted to add also functionality to
manage those catalog but the time was limited and I had to concentrate in the Vehicles API.

The implementation of securing the API is simple, users need to specify their user/password to get a token that they will use in 
the Authentication header in the request and users are hard-coded. Again, I had to implement the security this way because of the time I had but I just to give you
and idea I wanted to also create an API to manage users and create tokens for those API users (something like jwt).

I'm using Spring Boot, Spring Data KeyValue (using Maps for in memory storage) and Spring Hateoas for resources that describe
its relevant operations including links in the data returned. Since I constantly consume third party services in my job I think this kind
of API help a lot its consumer to understand what kind of operations can be executed on the resource.
 
I implemented a basic querying functionality leveraging the spring data abilities. But of course that will
be more elegant using SQL queries or something like that.

There is still missing the implementation of Paging the results and logging.

## Usage

### Login

There are 3 users already configured when the app starts.

* username=steve, password=12345, role=ADMIN, token=complexTokenForSteve
* username=woz, password=12345, role=USER, token=complexTokenForWoz
* username=tim, password=12345, role=VIEWER, token=complexTokenForTim

Users can login to get its `token` calling `http://localhost:9999/api/public/login`. This will return the `token` of the user.

### Authorization header

The `token` returned by the login endpoint should be use to call all other endpoints because they are secured and they need
the `Authorizacion` header with the value `Bearer [token]` where `token` is the returned by the login endpoint.

### Querying Vehicles as a VIEWER

`VIEWER` users can only use `http://localhost:9999/api/vehicles` to query the vehicles in the catalog. In the beginning viewer users
cannot find anything because all vehicles start with the status `IN_REVIEW`.

If you want to see vehicles as a `VIEWER` first you need to go and approve some vehicle calling the endpoint `http://localhost:9999/api/internal/vehicles`
as `ADMIN` user and find record with the status `IN_REVIEW` then call the endpoint `http://localhost:9999/api/internal/vehicles/{vehicleId}/approve` to
change the vehicle status from `IN_REVIEW` to `APPROVED`. Once this is done if you call `http://localhost:9999/api/vehicles` as `VIEWER`
you should see some results.

### Querying params

As any type of user the endpoints to query the vehicles can receive 2 params to filter the results:

* `year` - to only view those vehicles with the specified year.
* `manufacturerNameLike` - to filter those vehicles where the manufacturer name is like the specified string.

For example:

To query those vehicles with year 2015

```
curl -X GET \
 http://localhost:9999/api/internal/vehicles?year=2015 \
 -H 'Authorization: Bearer complexTokenForSteve' \
 -H 'cache-control: no-cache'
```

```
curl -X GET \
 http://localhost:9999/api/vehicles?year=2015 \
 -H 'Authorization: Bearer complexTokenForTim' \
 -H 'cache-control: no-cache'
```

or to query by manufacturer name:

```
curl -X GET \
 http://localhost:9999/api/internal/vehicles?manufacturerNameLike=Toy \
 -H 'Authorization: Bearer complexTokenForSteve' \
 -H 'cache-control: no-cache'
```

```
curl -X GET \
 http://localhost:9999/api/vehicles?manufacturerNameLike=Toy \
 -H 'Authorization: Bearer complexTokenForTim' \
 -H 'cache-control: no-cache'
```

or combination:

```
curl -X GET \
 http://localhost:9999/api/internal/vehicles?manufacturerNameLike=Toy&year=2015 \
 -H 'Authorization: Bearer complexTokenForSteve' \
 -H 'cache-control: no-cache'
```

```
curl -X GET \
 http://localhost:9999/api/vehicles?manufacturerNameLike=Toy&year=2015 \
 -H 'Authorization: Bearer complexTokenForTim' \
 -H 'cache-control: no-cache'
```

### Managing vehicles as USER/ADMIN

You can call endpoint `http://localhost:9999/api/internal/vehicles` as `USER` or `ADMIN` user to manage the vehicle catalog.

* GET `http://localhost:9999/api/internal/vehicles` to get all vehicles (no matter the vehicle state).
* GET `http://localhost:9999/api/internal/vehicles/{vehicleId}` to get one vehicle.
* DELETE `http://localhost:9999/api/internal/vehicles/{vehicleId}` to delete one vehicle.
* POST `http://localhost:9999/api/internal/vehicles` to add a new record to the vehicle catalog.
* PUT `http://localhost:9999/api/internal/vehicles/{vehicleId}` to update attributes in a vehicle (because of time you can only update by now: years, numDoors, numSeats, airCond, and transmissionType).
* POST `http://localhost:9999/api/internal/vehicles/{vehicleId}/approve` to change the status of a vehicle (this is only done by the `ADMIN` user).

To post data you must use this format:

```
{
  "engine": {
    "fuelType": {
      "id": <integer>
    },
    "numCylinders": <integer>
  },
  "year": <integer>,
  "numDoors": <integer>,
  "numSeats": <integer>,
  "airCond": <boolean>,
  "manufacturer": {
    "id": <integer>
  },
  "model": {
    "id": <integer>
  },
  "trim": {
    "id": <integer>
  },
  "transmissionType": <string -> "auto" or manual">,
  "fuelCapacity": {
    "capacity": <integer>,
    "unit": <string -> "liters" or "gallons">
  }
}
```

The app verifies the format of the fields and informs in case of an error. In the case of `manufacturer`, `model` and `trim` the data used
should be a correct combination, i.e., Trim should be associated with Model and Model should be associated with Manufacturer. You can 
query `http://localhost:9999/api/internal/manufacturers` to see the catalog and correct combinations of Manufacturer, Model and Trim.

The same applies for `fuelType`. The specified id should be one of `http://localhost:9999/api/internal/fueltypes`.



 






